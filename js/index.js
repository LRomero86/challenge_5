window.onload = inicial;
debugger;

function inicial() {
    var autos = ["Pickup Chevrolet 1928", "Coupe Torino 1975", "Mercury 47", "Renault Gordini 1969"];
    var autosRev = [];

    console.log('Iteraciones con FOR');
    console.log('');

    for (var i = 0; i < autos.length; i++) {

        autosRev.unshift(autos[i]);
        console.log(autos[i]);
    }

    autos.unshift("Chevrolet Chevy 1972");
    autosRev.unshift("Chevrolet Chevy 1972");

    console.log('-------------------------------------');

    console.log('Iteraciones con WHILE');
    console.log('');

    var i = 0;

    while (i < autosRev.length) {
        console.log(autosRev[i]);
        i++;
    }

    console.log('-------------------------------------');
    console.log('Usando Array.prototype.reverse()');
    console.log('');

    Array.prototype.reverse(autos);

    for (var i = 0; i < autos.length; i++) {
        console.log(autos[i]);
    }

    console.log('-------------------------------------');
    console.log('Usando Array.prototype.reverse() en autosRev');
    console.log('');

    Array.prototype.reverse(autosRev);

    i = 0;

    while (i < autosRev.length) {
        console.log(autosRev[i]);
        i++;
    }

    /*
    ejemplo de splice

    var fruits = ["Banana","Orange", "Apple", "Mango"];

    fruits.splice(2, 0, "Lemon", "Kiwi"); *** Agrega Lemon y Kiwi después de Orange ***

    var fruits = ["Banana","Orange", "Apple", "Mango"];

    fruits.splice(2, 1, "Lemon", "Kiwi");   *** Elimina Apple y agrega Lemon y Kiwi ***

    */
}